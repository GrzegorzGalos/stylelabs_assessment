﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test";  } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            XDocument xdocument = XDocument.Parse(xml);
            var grouppedParams = xdocument.Descendants("param").Select(m => new { Parameter = m.Attribute("name").Value, Value = double.Parse(m.Value, CultureInfo.InvariantCulture) })
                                                            .GroupBy(p => p.Parameter);

            foreach (var paramsGroup in grouppedParams)
            {
                var avarage = paramsGroup.Average(pg => pg.Value);
                var min = paramsGroup.Min(pg => pg.Value);
                var max = paramsGroup.Max(pg => pg.Value);
                PrintLine(paramsGroup.First().Parameter, min, avarage, max);
            }     
        }

        private void PrintLine(string paramName, double min, double avarage, double max)
        {            
            Console.WriteLine("{0} {1:0.0} {2:0.00} {3:0.0}", paramName.PadRight(15, ' '), min, avarage, max);
        }
    }
}
