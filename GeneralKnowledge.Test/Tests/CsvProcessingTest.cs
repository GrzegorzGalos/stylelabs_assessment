﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;
using GeneralKnowledge.Test.Model;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;
            var assets = ProcessText(csvFile);

        }

        private List<Asset> ProcessText(string csvFile)
        {
            var assets = new List<Asset>();
            TextReader textReader = new StringReader(csvFile);
            using (TextFieldParser csvParser = new TextFieldParser(textReader))
            {                
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    assets.Add(new Asset() {
                        Id = new Guid(fields[0]),
                        CreatedBy = fields[3],
                        Description = fields[6],
                        Email = fields[4],
                        FileName = fields[1],
                        Country = new Country(fields[5]),
                        MimeType = new MimeType(fields[2])
                    });
                }
            }
            return assets;
        }

    }

}
