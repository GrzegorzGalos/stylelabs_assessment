﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            var images = GenerateThumbnailAndPreviewFromUrl("http://www.timespages.com/wp-content/uploads/2015/11/Nature.jpg", 100, 80, 1200, 1600);
            //var images = GenerateThumbnailAndPreviewFromUrl("http://wallpoper.com/images/00/36/78/69/landscapes-nature_00367869.png", 100, 80, 1200, 1600);

            //Save thumbnail
            //SaveDataToDisk(@"C:\Users\ggalos\Pictures\thumbnail.png", images.Item1);
            //Save preview
            //SaveDataToDisk(@"C:\Users\ggalos\Pictures\preview.png", images.Item2);
        }

        private Tuple<byte[], byte[]> GenerateThumbnailAndPreviewFromUrl(string url, int thumbnailWidth, int thumbnailHeight, int previewWidth, int previewHeight)
        {
            var imageToResize = GetImageFromUrl(url);

            var thumbnail = ResizeImage(imageToResize, thumbnailWidth, thumbnailHeight);
            var preview = ResizeImage(imageToResize, previewWidth, previewHeight);

            var thumbnailData = ImageToByte(thumbnail);
            var previewData = ImageToByte(preview);
            return Tuple.Create(thumbnailData, previewData);

        }

        private Image GetImageFromUrl(string url)
        {
            Uri imageUri = new Uri(url);
            using (var client = new WebClient())
            {
                var imageData = client.DownloadData(imageUri);
                var imageToResize = ConvertByteArrayToImage(imageData);
                return imageToResize;
            }
        }

        private Image ConvertByteArrayToImage(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream(byteArray, 0, byteArray.Length);
            ms.Write(byteArray, 0, byteArray.Length);
            var image = Image.FromStream(ms);

            return image;
        }

        private Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private byte[] ImageToByte(Image img)
        {
            byte[] byteArray = new byte[0];
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        private void SaveDataToDisk(string FileName, byte[] Data)
        {
            BinaryWriter Writer = null;
            using (Writer = new BinaryWriter(File.OpenWrite(FileName)))
            {              
                Writer.Write(Data);
                Writer.Flush();
                Writer.Close();
            }
        }
    }

}
