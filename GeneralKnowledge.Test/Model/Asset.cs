﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.Model
{
    public class Asset
    {
        private DateTime? _createdOn;
        
        public Guid Id { get; set; }
        public MimeType MimeType { get; set; }
        public Country Country { get; set; }
        public string FileName { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }

        public DateTime CreatedOn
        {
            get
            {
                return _createdOn.HasValue ? _createdOn.Value : DateTime.Now;
            }
            set
            {
                _createdOn = value;
            }
        }
    }
}
