﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.Model
{
    public class Country
    {
        public string Name { get; set; }

        public Country() { }

        public Country(string name):this() {
            Name = name;
        }
    }
}
