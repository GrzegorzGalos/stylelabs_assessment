﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.Model
{
    public class MimeType
    {
        public string Name { get; set; }

        public MimeType() { }

        public MimeType(string name):this() {
            Name = name;
        }
    }
}
