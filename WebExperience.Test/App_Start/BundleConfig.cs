﻿using System.Web;
using System.Web.Optimization;

namespace WebExperience.Test
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var scripts = new ScriptBundle("~/bundles/scripts");            
            bundles.Add(scripts.Include(
                        "~/Scripts/jquery/jquery-1.10.2.js",
                        "~/Scripts/notify.js",
                        "~/Scripts/jquery.blockUI.js",
                        
                        "~/Scripts/angular/angular.js",
                        "~/Scripts/angular/angular-route.js",

                        "~/Scripts/TypeScript/IAsset.js",

                        "~/Scripts/TypeScript/Services/Assets.js",
                        "~/Scripts/TypeScript/Services/Notify.js",
                        "~/Scripts/TypeScript/Services/Services.js",

                        "~/Scripts/TypeScript/App.js",
                        "~/Scripts/TypeScript/Controllers/Index.js",
                        "~/Scripts/TypeScript/Controllers/New.js",
                        "~/Scripts/TypeScript/Controllers/Edit.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            //BundleTable.EnableOptimizations = true;
        }
    }
}
