﻿using Ninject.Modules;
using Ninject.Web;
using Ninject.Activation;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebExperience.Test.Models.Repository;

namespace WebExperience.Test.Models.Infrastructure
{
    public class WebBinding : NinjectModule
    {
        public override void Load()
        {
            Bind<IAssetsRepository>().To<AssetsRepository>().InRequestScope();
        }

    }
}