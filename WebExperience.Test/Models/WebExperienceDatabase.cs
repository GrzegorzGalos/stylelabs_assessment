﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using GeneralKnowledge.Test.Model;

namespace WebExperience.Test.Models
{
    public class WebExperienceDatabase : DbContext
    {

        public WebExperienceDatabase() : base("DefaultConnection")
        {
        }

        public DbSet<Asset> Assets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}