﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GeneralKnowledge.Test.Model;
using System.Data.Entity;

namespace WebExperience.Test.Models.Repository
{
    public class AssetsRepository : IAssetsRepository
    {
        WebExperienceDatabase _dbContext;
        public AssetsRepository()
        {
            _dbContext = new WebExperienceDatabase();
        }

        public IQueryable<Asset> Assets
        {
            get
            {
                return _dbContext.Assets;
            }
        }

        public void Insert(Asset asset)
        {
            asset.Id = Guid.NewGuid();
            this._dbContext.Assets.Add(asset);
        }

        public void Update(Asset asset)
        {
            _dbContext.Entry(asset).State = EntityState.Modified;
        }
        
        public void DeleteAsset(Guid id)
        {
            this._dbContext.Assets.Remove(this._dbContext.Assets.First(f => f.Id == id));
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

    }
}