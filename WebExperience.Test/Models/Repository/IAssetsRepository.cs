﻿using GeneralKnowledge.Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebExperience.Test.Models.Repository
{
    public interface IAssetsRepository
    {
        IQueryable<Asset> Assets { get; }
        void Insert(Asset asset);
        void Update(Asset asset);
        void DeleteAsset(Guid id);
        void Save();
    }
}
