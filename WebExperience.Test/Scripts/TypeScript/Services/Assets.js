var WebExperience;
(function (WebExperience) {
    var Services;
    (function (Services) {
        var AssetsService = (function () {
            function AssetsService($http, $q) {
                this.$http = $http;
                this.$q = $q;
            }
            AssetsService.prototype.GetAll = function (skip, take) {
                return this.GetRequest("/api/Assets/" + skip + "/" + take, {});
            };
            AssetsService.prototype.Get = function (id) {
                return this.GetRequest("/api/Assets/" + id, {});
            };
            AssetsService.prototype.Create = function (Item) {
                var dfr = this.$q.defer();
                this.$http.post("/api/Assets/Post", Item, {
                    responseType: 'json'
                }).then(function (result) {
                    dfr.resolve(result.data);
                }, function (result) {
                    dfr.reject(result.data);
                });
                return dfr.promise;
            };
            AssetsService.prototype.Update = function (Item) {
                var dfr = this.$q.defer();
                this.$http.put("/api/Assets/Put", Item, {
                    responseType: 'json'
                }).then(function (result) {
                    dfr.resolve(result.data);
                }, function (result) {
                    dfr.reject(result.data);
                });
                return dfr.promise;
            };
            AssetsService.prototype.Delete = function (id) {
                var dfr = this.$q.defer();
                this.$http.delete("/api/Assets/" + id, {
                    responseType: 'json'
                }).then(function (result) {
                    dfr.resolve(result.data);
                }, function (result) {
                    dfr.reject(result.data);
                });
                return dfr.promise;
            };
            AssetsService.prototype.GetRequest = function (url, args) {
                var dfr = this.$q.defer();
                this.$http.get(url, {
                    params: args,
                    responseType: 'json'
                }).then(function (result) {
                    dfr.resolve(result.data);
                }, function (result) {
                    dfr.reject(result.data);
                });
                return dfr.promise;
            };
            AssetsService.$inject = ["$http", '$q'];
            return AssetsService;
        }());
        Services.AssetsService = AssetsService;
    })(Services = WebExperience.Services || (WebExperience.Services = {}));
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=Assets.js.map