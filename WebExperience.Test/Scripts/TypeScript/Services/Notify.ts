﻿module WebExperience.Services {

    export class NotifyService {
        static $inject = [];
        constructor() {

        }

        public Error(msg: string) {
            (<any>$).notify(msg, "error")
        }

        public Notify(msg: string) {
            (<any>$).notify(msg);
        }

        public BlockUI() {
            (<any>$).blockUI();
        }
        public UnblockUI() {
            (<any>$).unblockUI();
        }
    }
}
