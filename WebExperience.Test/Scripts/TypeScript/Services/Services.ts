﻿module WebExperience.Services {
    angular.module('WebExperienceServices', []).service("assetsService", WebExperience.Services.AssetsService);
    angular.module('WebExperienceServices').service("notifyService", WebExperience.Services.NotifyService);
}
