﻿module WebExperience.Services {
    
    export class AssetsService  {
        static $inject = ["$http", '$q'];
        constructor(private $http: angular.IHttpService, private $q: angular.IQService) {

        }

        public GetAll(skip: number, take: number): angular.IPromise<Array<any>> {
            return this.GetRequest("/api/Assets/" + skip + "/" + take, {});
        }

        public Get(id: string): angular.IPromise<Array<IAsset>> {
            return this.GetRequest("/api/Assets/" + id, {});
        }

        public Create(Item: any): angular.IPromise<Array<boolean>> {
            var dfr = this.$q.defer();

            this.$http.post("/api/Assets/Post", Item, {
                responseType: 'json'
            }).then(function (result) {
                dfr.resolve(result.data)
            }, function (result) {
                dfr.reject(result.data);
            });
            return dfr.promise;
        }

        public Update(Item: any): angular.IPromise<Array<boolean>> {
            var dfr = this.$q.defer();

            this.$http.put("/api/Assets/Put", Item, {
                responseType: 'json'
            }).then(function (result) {
                dfr.resolve(result.data)
            }, function (result) {
                dfr.reject(result.data);
            });
            return dfr.promise;
        }

        public Delete(id: string): angular.IPromise<Array<boolean>> {
            var dfr = this.$q.defer();

            this.$http.delete("/api/Assets/" + id, {
                responseType: 'json'
            }).then(function (result) {
                dfr.resolve(result.data)
            }, function (result) {
                dfr.reject(result.data);
            });
            return dfr.promise;
        }

        private GetRequest(url: string, args: any): angular.IPromise<Array<any>> {
            var dfr = this.$q.defer();

            this.$http.get(url, {
                params: args,
                responseType: 'json'
            }).then(function (result) {
                dfr.resolve(result.data)
            }, function (result) {
                dfr.reject(result.data);
            });
            return dfr.promise;
        }
    }
}
