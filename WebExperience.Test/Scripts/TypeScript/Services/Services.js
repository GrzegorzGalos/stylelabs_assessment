var WebExperience;
(function (WebExperience) {
    var Services;
    (function (Services) {
        angular.module('WebExperienceServices', []).service("assetsService", WebExperience.Services.AssetsService);
        angular.module('WebExperienceServices').service("notifyService", WebExperience.Services.NotifyService);
    })(Services = WebExperience.Services || (WebExperience.Services = {}));
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=Services.js.map