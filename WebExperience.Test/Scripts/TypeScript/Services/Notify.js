var WebExperience;
(function (WebExperience) {
    var Services;
    (function (Services) {
        var NotifyService = (function () {
            function NotifyService() {
            }
            NotifyService.prototype.Error = function (msg) {
                $.notify(msg, "error");
            };
            NotifyService.prototype.Notify = function (msg) {
                $.notify(msg);
            };
            NotifyService.prototype.BlockUI = function () {
                $.blockUI();
            };
            NotifyService.prototype.UnblockUI = function () {
                $.unblockUI();
            };
            NotifyService.$inject = [];
            return NotifyService;
        }());
        Services.NotifyService = NotifyService;
    })(Services = WebExperience.Services || (WebExperience.Services = {}));
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=Notify.js.map