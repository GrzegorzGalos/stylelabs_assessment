﻿
module WebExperience {

    export class EditController extends NewController {
        public Item: IAsset;
        static $inject = ['$scope', '$q', 'assetsService', '$location', '$routeParams', 'notifyService'];

        constructor(protected $scope: angular.IScope,
            protected $q: angular.IQService,
            protected assetsService: WebExperience.Services.AssetsService,
            protected $location: angular.ILocationService,
            protected $routeParams: any,
            protected notifyService: WebExperience.Services.NotifyService) {
            super($scope, $q, assetsService, $location, notifyService);

            var self = this;
            self.LoadItem();
        }
        public LoadItem() {
            var self = this;

            self.notifyService.BlockUI();
            self.assetsService.Get(self.$routeParams.Id).then(data => {
                self.notifyService.UnblockUI();
                self.Item = data;
            }, fail => {
                self.notifyService.UnblockUI();
                self.notifyService.Error("There was an error while connecting to server");
            })
        }

        public Save() {
            var self = this;
            if (self.Validate()) {
                self.notifyService.BlockUI();
                self.assetsService.Update(self.Item).then(success => {
                    self.notifyService.UnblockUI();
                    self.$location.path("/");
                }, fail => {
                    self.notifyService.UnblockUI();
                    self.notifyService.Error("There was an error while connecting to server");
                });
            }
        }
    }

    var app = angular.module("assetsApp").controller('Edit', EditController);
}