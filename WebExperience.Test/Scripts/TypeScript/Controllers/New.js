var WebExperience;
(function (WebExperience) {
    var NewController = (function () {
        function NewController($scope, $q, assetsService, $location, notifyService) {
            this.$scope = $scope;
            this.$q = $q;
            this.assetsService = assetsService;
            this.$location = $location;
            this.notifyService = notifyService;
            var self = this;
            self.Item = { Country: {}, MimeType: {} };
        }
        NewController.prototype.Validate = function () {
            var self = this;
            var form = self.$scope.itemForm;
            form.$setSubmitted();
            return form.$valid;
        };
        NewController.prototype.Save = function () {
            var self = this;
            if (self.Validate()) {
                self.notifyService.BlockUI();
                self.assetsService.Create(self.Item).then(function (success) {
                    self.notifyService.UnblockUI();
                    self.$location.path("/");
                }, function (fail) {
                    self.notifyService.UnblockUI();
                    self.notifyService.Error("There was an error while connecting to server");
                });
            }
        };
        NewController.$inject = ['$scope', '$q', 'assetsService', '$location', 'notifyService'];
        return NewController;
    }());
    WebExperience.NewController = NewController;
    var app = angular.module("assetsApp").controller('New', NewController);
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=New.js.map