﻿
module WebExperience {

    export class NewController {
        public Item: IAsset;
        static $inject = ['$scope', '$q', 'assetsService', '$location', 'notifyService'];
        constructor(protected $scope: angular.IScope,
            protected $q: angular.IQService,
            protected assetsService: WebExperience.Services.AssetsService,
            protected $location: angular.ILocationService,
            protected notifyService: WebExperience.Services.NotifyService) {
            var self = this;

            self.Item = { Country: {}, MimeType: {}};
        }

        public Validate(): boolean {
            var self = this;
            var form: any = (<any>self.$scope).itemForm;
            form.$setSubmitted();

            return form.$valid;
        }

        public Save() {
            var self = this;
            if (self.Validate()) {
                self.notifyService.BlockUI();
                self.assetsService.Create(self.Item).then(success => {
                    self.notifyService.UnblockUI();
                    self.$location.path("/");
                }, fail => {
                    self.notifyService.UnblockUI();
                    self.notifyService.Error("There was an error while connecting to server");
                });
            }
        }
    }

    var app = angular.module("assetsApp").controller('New', NewController);
}