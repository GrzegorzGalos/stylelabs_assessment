﻿
module WebExperience {

    export class IndexController {
        public Items: any;
        public Pages: any;
        public CurrentPage: any;
        public ItemsPerPage: any;
        public TotalNumberOfItems: any;
        public ShowPagination: boolean;
        
        static $inject = ['$scope', '$q', 'assetsService', 'notifyService'];
        constructor(private $scope: angular.IScope,
            private $q: angular.IQService,
            private assetsService: WebExperience.Services.AssetsService,
            private notifyService: WebExperience.Services.NotifyService) {
            var self = this;
            self.CurrentPage = 0;
            self.ItemsPerPage = 5;
            self.Pages = [];
            self.RelaodData();
        }

        public RecalculatePagination() {
            var self = this;
            self.Pages = [];
            
            var pageNumber = 1;
            for (var page = 0; page < self.TotalNumberOfItems; page += self.ItemsPerPage) {
                self.Pages.push(pageNumber);
                pageNumber++;
            }
            self.ShowPagination = pageNumber > 2;

        }

        public PrevPage() {
            var self = this;
            if (self.CurrentPage > 0) {
                self.CurrentPage--;
                self.RelaodData();
            }
        }

        public OpenPage(page: number) {
            var self = this;
            self.CurrentPage = page - 1;
            self.RelaodData();
        }

        public NextPage() {
            var self = this;
            self.CurrentPage++;
            self.RelaodData();
        }

        public RelaodData() {
            var self = this;
            self.notifyService.BlockUI();
            self.assetsService.GetAll((self.CurrentPage * self.ItemsPerPage), self.ItemsPerPage).then((data: any) => {
                self.notifyService.UnblockUI();
                self.Items = data.Items;
                self.TotalNumberOfItems = data.NumberOfItems;
                self.RecalculatePagination();
            }, fail => {
                self.notifyService.UnblockUI();
                self.notifyService.Error("There was an error while connecting to server");
            })
        }

        public Delete(id: string) {
            var self = this;
            self.assetsService.Delete(id).then(data => {
                self.CurrentPage = 0;
                self.RelaodData();
            }, fail => {

            })
        }
    }

    var app = angular.module("assetsApp").controller('Index', IndexController);
}