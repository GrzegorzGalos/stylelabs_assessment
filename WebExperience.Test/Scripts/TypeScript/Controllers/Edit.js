var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var WebExperience;
(function (WebExperience) {
    var EditController = (function (_super) {
        __extends(EditController, _super);
        function EditController($scope, $q, assetsService, $location, $routeParams, notifyService) {
            _super.call(this, $scope, $q, assetsService, $location, notifyService);
            this.$scope = $scope;
            this.$q = $q;
            this.assetsService = assetsService;
            this.$location = $location;
            this.$routeParams = $routeParams;
            this.notifyService = notifyService;
            var self = this;
            self.LoadItem();
        }
        EditController.prototype.LoadItem = function () {
            var self = this;
            self.notifyService.BlockUI();
            self.assetsService.Get(self.$routeParams.Id).then(function (data) {
                self.notifyService.UnblockUI();
                self.Item = data;
            }, function (fail) {
                self.notifyService.UnblockUI();
                self.notifyService.Error("There was an error while connecting to server");
            });
        };
        EditController.prototype.Save = function () {
            var self = this;
            if (self.Validate()) {
                self.notifyService.BlockUI();
                self.assetsService.Update(self.Item).then(function (success) {
                    self.notifyService.UnblockUI();
                    self.$location.path("/");
                }, function (fail) {
                    self.notifyService.UnblockUI();
                    self.notifyService.Error("There was an error while connecting to server");
                });
            }
        };
        EditController.$inject = ['$scope', '$q', 'assetsService', '$location', '$routeParams', 'notifyService'];
        return EditController;
    }(WebExperience.NewController));
    WebExperience.EditController = EditController;
    var app = angular.module("assetsApp").controller('Edit', EditController);
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=Edit.js.map