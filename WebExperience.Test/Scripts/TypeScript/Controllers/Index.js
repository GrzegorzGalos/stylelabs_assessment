var WebExperience;
(function (WebExperience) {
    var IndexController = (function () {
        function IndexController($scope, $q, assetsService, notifyService) {
            this.$scope = $scope;
            this.$q = $q;
            this.assetsService = assetsService;
            this.notifyService = notifyService;
            var self = this;
            self.CurrentPage = 0;
            self.ItemsPerPage = 5;
            self.Pages = [];
            self.RelaodData();
        }
        IndexController.prototype.RecalculatePagination = function () {
            var self = this;
            self.Pages = [];
            var pageNumber = 1;
            for (var page = 0; page < self.TotalNumberOfItems; page += self.ItemsPerPage) {
                self.Pages.push(pageNumber);
                pageNumber++;
            }
            self.ShowPagination = pageNumber > 2;
        };
        IndexController.prototype.PrevPage = function () {
            var self = this;
            if (self.CurrentPage > 0) {
                self.CurrentPage--;
                self.RelaodData();
            }
        };
        IndexController.prototype.OpenPage = function (page) {
            var self = this;
            self.CurrentPage = page - 1;
            self.RelaodData();
        };
        IndexController.prototype.NextPage = function () {
            var self = this;
            self.CurrentPage++;
            self.RelaodData();
        };
        IndexController.prototype.RelaodData = function () {
            var self = this;
            self.notifyService.BlockUI();
            self.assetsService.GetAll((self.CurrentPage * self.ItemsPerPage), self.ItemsPerPage).then(function (data) {
                self.notifyService.UnblockUI();
                self.Items = data.Items;
                self.TotalNumberOfItems = data.NumberOfItems;
                self.RecalculatePagination();
            }, function (fail) {
                self.notifyService.UnblockUI();
                self.notifyService.Error("There was an error while connecting to server");
            });
        };
        IndexController.prototype.Delete = function (id) {
            var self = this;
            self.assetsService.Delete(id).then(function (data) {
                self.CurrentPage = 0;
                self.RelaodData();
            }, function (fail) {
            });
        };
        IndexController.$inject = ['$scope', '$q', 'assetsService', 'notifyService'];
        return IndexController;
    }());
    WebExperience.IndexController = IndexController;
    var app = angular.module("assetsApp").controller('Index', IndexController);
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=Index.js.map