var WebExperience;
(function (WebExperience) {
    var assetsApp = angular.module("assetsApp", ['ngRoute', 'WebExperienceServices']);
    assetsApp.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
            redirectTo: '/Index'
        })
            .when('/Index', {
            templateUrl: '/Index.html',
            controller: 'Index',
            controllerAs: 'main'
        })
            .when('/Edit/:Id', {
            templateUrl: '/Edit.html',
            controller: 'Edit',
            controllerAs: 'main'
        })
            .when('/New', {
            templateUrl: '/Edit.html',
            controller: 'New',
            controllerAs: 'main'
        });
    });
})(WebExperience || (WebExperience = {}));
//# sourceMappingURL=App.js.map