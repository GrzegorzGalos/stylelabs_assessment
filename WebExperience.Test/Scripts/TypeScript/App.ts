﻿
module WebExperience {

    var assetsApp = angular.module("assetsApp", ['ngRoute', 'WebExperienceServices']);
    // configure our routes
    assetsApp.config(function ($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/', {
                redirectTo: '/Index'
            })
            // route for the contact page
            .when('/Index', {
                templateUrl: '/Index.html',
                controller: 'Index',
                controllerAs: 'main'
            })
            // route for the contact page
            .when('/Edit/:Id', {
                templateUrl: '/Edit.html',
                controller: 'Edit',
                controllerAs: 'main'
            })
            // route for the contact page
            .when('/New', {
                templateUrl: '/Edit.html',
                controller: 'New',
                controllerAs: 'main'
            })

    });
}