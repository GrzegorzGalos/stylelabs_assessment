﻿
module WebExperience {

    export interface IAsset {        
        Id?: string,
        MimeType?: IDictionary,
        Country?: IDictionary,
        FileName?: string,
        CreatedBy?: string,
        CreatedOn?: string,
        Email?: string,
        Description?: string,
    }

    export interface IDictionary {
        Name?: string,
    }
    
}