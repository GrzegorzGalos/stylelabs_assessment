﻿using GeneralKnowledge.Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebExperience.Test.Models.Repository;

namespace WebExperience.Test.Controllers.api
{
    public class AssetsController : ApiController
    {
        private IAssetsRepository _repository;
        
        public AssetsController(IAssetsRepository repository)
        {
            _repository = repository;
        }
        
        public object Get(int take=5, int skip=0)
        {
            return new {
                            NumberOfItems = _repository.Assets.Count(),
                            Items = _repository.Assets.OrderBy(a => a.CreatedOn)
                            .Skip(skip).Take(take)
                            .Select(a => new { FileName = a.FileName, CreatedOn = a.CreatedOn, Id = a.Id.ToString() })
                        } ;
        }
        
        public Asset Get(string id)
        {
            return _repository.Assets.First(a => a.Id == new Guid(id));
        }
        
        [HttpPost]
        public void Post([FromBody]Asset asset)
        {
            _repository.Insert(asset);
            _repository.Save();
        }
        
        [HttpPut]
        public void Put([FromBody]Asset asset)
        {
            _repository.Update(asset);
            _repository.Save();

        }
        
        [HttpDelete]
        public void Delete(string id)
        {
            _repository.DeleteAsset(new Guid(id));
            _repository.Save();
        }
    }
}
